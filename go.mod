module bitbucket.org/AaronTWilson/ihcf

go 1.16

require (
	github.com/pkg/errors v0.9.1
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
)
